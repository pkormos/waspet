'''
created by:     Patrick Kormos
    started:    2024 June 27
'''

import sys,os,shutil               #
sys.path.append('D:/projects/getData/') #
import requests
from pandas import DataFrame,concat,to_datetime,set_option,read_csv
set_option('display.max_rows',2000)
set_option('display.max_columns',40)
from datetime import datetime as dt
from datetime import timedelta
import numpy as np

class evalFcstPt:

    def __init__(self,optionsDict,clobber=False):
        '''
        initiate class function. maybe get information on length of runoff volume record. hit roadblock bc limit of 2000 records on API 
            inputs:     options dictionary with (for example): 
                'stnTriplet':         '06099500:MT:USGS', # station triplet
                'fcstMo':             1,                  # forecast month int 1=January
                'fcstDay':            1,                  # forecast day int. 1=1st, 15=15th
                'trainingStartYear':  1991,               # start calibration year
                'trainingEndYear':    2020,               # end calibration year
                'fcstPdSt':           4,                  # april  This is the runoff period we are forecasting
                'fcstPdEt':           7,                  # july   This is the runoff period we are forecasting
                'wteqStnLst':       ['307:MT:SNTL','693:MT:SNTL','458:MT:SNTL'], # list of swe stns to be used in training
                'precStnLst':       ['307:MT:SNTL','693:MT:SNTL','458:MT:SNTL'], # list of precipitation stations to be used in training
                'ga':               False,                # whether to use the genetic algorithm in M4
                'evalData':         'grace_tws',        # list of data to be evaluated in forecasting framework can be empty list
                'm4baseDir':        'D:/projects/M4/'

 
        '''
        if not optionsDict:
            print('user must provide a dictionary with model options.')
            exit(0)
        self.optionsDict = optionsDict                      # make avail to functions
        self.tripDf = namesOfTrip([optionsDict['stnTriplet']])   # get name of triplet
        print(self.tripDf)

        ################## directory work ##################
        if optionsDict['evalData']:                         # if we are evaluating a dataset (list not empty)
            if optionsDict['ga']:
                dirName = os.path.join(optionsDict['m4baseDir'],self.optionsDict['evalData'],f'{self.tripDf.loc[0,'name']}_{optionsDict['fcstMo']}-{optionsDict['fcstDay']}_ga').replace("\\","/")
            else:
                dirName = os.path.join(optionsDict['m4baseDir'],self.optionsDict['evalData'],f'{self.tripDf.loc[0,'name']}_{optionsDict['fcstMo']}-{optionsDict['fcstDay']}').replace("\\","/")
        else:                                               # just base eval with swe and precip
            if optionsDict['ga']:
                dirName = os.path.join(optionsDict['m4baseDir'],'base',f'{self.tripDf.loc[0,'name']}_{optionsDict['fcstMo']}-{optionsDict['fcstDay']}_ga').replace("\\","/")
            else:
                dirName = os.path.join(optionsDict['m4baseDir'],'base',f'{self.tripDf.loc[0,'name']}_{optionsDict['fcstMo']}-{optionsDict['fcstDay']}').replace("\\","/")

        print(dirName)
        if os.path.exists(dirName):
            print('Directory exists.')
        

        self.dirName=dirName
        ######################################################
        # exit(0)

    def pullData(self):
        # pull fcsts, maybe use for performance metric later?
        fcst,dur = getForecast(self.optionsDict['trainingEndYear'],self.tripDf)   # mostly just to get duration text string for getData function

        # pull obs
        obsSt = dt(self.optionsDict['trainingStartYear'],self.optionsDict['fcstPdSt'],1)
        obsEt = dt(self.optionsDict['trainingEndYear'], (self.optionsDict['fcstPdEt']+1),1)
        obs = getData(self.tripDf,obsSt,obsEt,dur=dur)                       # establish srvo data class
        obs = obs[obs['month'].isin(np.arange(self.optionsDict['fcstPdSt'],(self.optionsDict['fcstPdEt']+1)))]  # keep months in runoff period
        obs.loc[:,'day']=1                                              # any day to make datetime
        obs.loc[:,'Year'] = to_datetime(obs[['year','month','day']])    # calculate date time column
        obs.drop(columns=['month','day','year'],inplace=True)           # clean up df for merge
        obs.set_index('Year',inplace=True)                              # set time index
        obs = obs.groupby(obs.index.year).sum(min_count=self.optionsDict['fcstPdEt']-self.optionsDict['fcstPdSt']+1) # sum monthly runoff by year, must have all monthly vals
        # print(obs)

        # print('pull swe predictor vars')
        predSt = dt(self.optionsDict['trainingStartYear'],self.optionsDict['fcstMo'],self.optionsDict['fcstDay'])
        predEt = dt(self.optionsDict['trainingEndYear'],self.optionsDict['fcstMo']+1,self.optionsDict['fcstDay'])
        wTripDf = namesOfTrip(self.optionsDict['wteqStnLst'],element='WTEQ')
        wteq = getData(wTripDf,predSt,predEt,dur=dur,element='WTEQ')     # pull first of month swe data from awdb
        wteq = wteq[wteq['month']==self.optionsDict['fcstMo']]           # only keep first of forecast month data
        if (dur=='SEMIMONTHLY') & (self.optionsDict['fcstDay'] in (15,16)):                       # need to be able to get daily data for daily forecasts.
            wteq = wteq[wteq['monthPart']=='2']
        wteq.set_index('year',inplace=True)
        wteq.drop(columns=['month'],inplace=True)
        # print(wteq)
        # exit(0)

        pTripDf = namesOfTrip(self.optionsDict['precStnLst'],element='PREC')
        prec = getData(pTripDf,predSt,predEt,dur=dur,element='PREC')                        # pull accumulated wy precp data from awdb
        prec = prec[prec['month']==self.optionsDict['fcstMo']]                              # only keep first of forecast month data
        if (dur=='SEMIMONTHLY') & (self.optionsDict['fcstDay'] in (15,16)):                 # need to be able to get daily data for daily forecasts.
            prec = prec[prec['monthPart']=='2']
        prec.set_index('year',inplace=True)
        prec.drop(columns=['month'],inplace=True)
        # print(prec)
        # exit(0)

        trainingFile = obs.merge(wteq,left_index=True,right_index=True,how='outer')
        self.trainingFile=trainingFile.merge(prec,left_index=True,right_index=True,how='outer')
        # print(self.trainingFile)
        # exit(0)
        if self.optionsDict['evalData']:         # if list not empty
            # print(self.optionsDict['evalData'])
            if 'grace_tws' in self.optionsDict['evalData']:
                graceTws = getGrace(self.optionsDict['stnTriplet'],
                    self.optionsDict['fcstMo'],
                    self.optionsDict['fcstDay'],
                    var='TWDSGZZ',
                    sy=self.optionsDict['trainingStartYear'],
                    ey=self.optionsDict['trainingEndYear'])
                graceTws.set_index('cal_yr',inplace=True)
                graceTws = graceTws[['val']]
                graceTws.rename(columns={'val':f'{self.tripDf["name"][0]}_TWS'},inplace=True)
                # print(graceTws)
                # exit(0)
                self.trainingFile = self.trainingFile.merge(graceTws,left_index=True,right_index=True,how='outer')
                # self.trainingFile.drop(columns=['month'],inplace=True)
                # print(self.trainingFile)
                # exit(0)
        return(self.trainingFile)

    def m4setup(self,trainingFileName=None,dropMissing=True,clobber=False):
        '''
        function to write create directories, copy r files (symlinks), and write training file
            input:  trainingFileName: filename for training file if non standard, None, default is standard training file name
                    dropMissing: boolean to drop rows with missing values. False only to be used if looking at training data
                                for some reason, model will fail (i think) with missing data in rows. Leave True if running model
                    clobber: whether to overwrite a directory from a previous run
            returns: training dataFrame
        '''
        # directory and file work
        srcDir = 'D:/projects/M4/cleanDir'
        
        if os.path.exists(self.dirName):
            if clobber==True:
                shutil.rmtree(self.dirName)
            else:
                print('Directory exists and clobber is False. Please reconcile.')
                exit(0)
        os.makedirs(self.dirName)
        cleanFileLst = os.listdir(srcDir)
        cleanFileLst = [x for x in cleanFileLst if 'MMPE_RunControlFile.txt' not in x]
        for file0 in cleanFileLst:
            os.symlink(os.path.join(srcDir,file0),os.path.join(self.dirName,file0))
        shutil.copyfile(os.path.join(srcDir,'MMPE_RunControlFile.txt'),os.path.join(self.dirName,'MMPE_RunControlFile.txt'))

        if not trainingFileName:
            trainingFileName=os.path.join(self.dirName,'MMPEInputData_ModelBuildingMode.txt')
        if dropMissing:
            self.trainingFile.dropna(axis=0,inplace=True)
        self.trainingFile.to_csv(trainingFileName,sep='\t',index_label='Year')
        return(self.trainingFile)

    def runM4(self):
        '''
        function to run M4
            inputs: ga: True for running genetic algorithm, which takes a long time. False for not running it and using all predictor vars
        '''
        import subprocess as sp

        ################## directory work ##################
        # if clobber==False:                                  # don't overwrite dir if it exists
        #     try:
        #         shutil.copytree(srcDir,self.dirName)
        #     except:
        #         print(f'Directory exists: {self.dirName}')
        #         print('If you want to overwrite it, add the clobber=True option to evalFcstPt class.')
        #         exit(0)
        # if clobber==True:
        #         print(f'Overwriting directory: {self.dirName}')
        #         shutil.rmtree(self.dirName)
        #         shutil.copytree(srcDir,self.dirName)
        ######################################################

        origDir = os.getcwd()
        # print(origDir)
        os.chdir(self.dirName)
        print(os.getcwd())

        # modify run contol file with ga option
        ifn = 'MMPE_RunControlFile.txt'     # M4 control filename
        f = open(ifn,'r')                   # open existing control file
        txt0 = f.read()                     # read in contents as variable
        tt1 = txt0.index('GeneticAlgorithmFlag')    # find ga flag location 
        if self.optionsDict['ga']==True:
            txt0 = txt0[:tt1-15] + 'Y' + txt0[tt1-14:]            # replace old date with new date, overwrite txt var
        else:
            txt0 = txt0[:tt1-15] + 'N' + txt0[tt1-14:]            # replace old date with new date, overwrite txt var
        f.close()                                                   # close the file


        f = open(ifn,'w')                                           # reopen the file as write (overwrites old file)
        f.write(txt0)                                               # write new txt
        f.close()                                                   # close file, which really writes text

        # rVersion = os.listdir('C:/Program Files/R')[0]
        # cmd0 = f'"C:/Program Files/R/{rVersion}/bin/x64/Rscript.exe" "MMPE-Main_MkII.R"'
        cmd0 = r'"C:/Program Files/R/R-4.4.1/bin/x64/Rscript.exe" "MMPE-Main_MkII.R"'
        with open("m4run.log", "wb") as f:
            print('before command')
            process = sp.Popen(cmd0, stdout=sp.PIPE)
            print('after command')
            print(process)
            for c in iter(lambda: process.stdout.read(1), b""):
                sys.stdout.buffer.write(c)
                f.write(c)
        os.chdir(origDir)

def getData(tripDf,st,et,dur='MONTHLY',element='SRVO'):
    '''
    function to get observed data (not forecasts)
    inputs:
        st: start time (datetime object)
        et: end time (datetime object)
        dur: duration (something like SEMIMONTHLY)
    returns: dataframe of obs
    '''
    trip = ','.join(tripDf['trip'].tolist()) # combine into comma separated list
    baseUrl = 'https://wcc.sc.egov.usda.gov/awdbRestApi/services/v1/data?'
    dataUrl =f'{baseUrl}beginDate={st}&endDate={et}&elements={element}&duration={dur}&stationTriplets={trip}' 
    dReq = requests.get(dataUrl)                # send request for data
    dRes = dReq.json()                          # get requested data in json fmt (list of stations)

    if not dRes:                                # if no obs
        return(DataFrame())                     # return an empty DataFrame
    for i,li in enumerate(dRes):
        liName = tripDf.loc[tripDf.trip==li['stationTriplet'],'name'].values[0] # extract name from tripDf using triplet
        for j,d in enumerate(li['data']):
            dCode = d['stationElement']['elementCode']
            dDf0 = DataFrame(d['values'])
            dDf0.rename(columns={'value':f'{liName}_{dCode}'},inplace=True)
            if (i==0)&(j==0):
                dDf=dDf0.copy(deep=True)
            else:
                if dur=='SEMIMONTHLY':
                    dDf = dDf.merge(dDf0,left_on=['month','monthPart','year'],right_on=['month','monthPart','year'],how='outer')
                    # dDf['monthPart'] = dDf.monthPart.astype(int)
                elif dur=='MONTHLY':
                    dDf = dDf.merge(dDf0,left_on=['month','year'],right_on=['month','year'],how='outer')
    return(dDf)

def getGrace(site,fcstMo,fcstDay,var='TWDSGZZ',sy=None,ey=None):
    '''
    function to get grace data from exdaily, the sqlite experimental daily database in the GriDAT dir ../
    '''
    import sqlite3
    from pandas import read_sql
    station = site.split(':')[0]
    state = site.split(':')[1]
    sqlStmnt = f"Select cal_yr,mo,day,val FROM exdaily WHERE cal_yr>={sy} AND cal_yr<={ey} AND shef='{var}' AND mo={fcstMo} AND day={fcstDay} AND station='{station}' AND state='{state}';"
    # print(sqlStmnt)
    con=sqlite3.connect('D:/projects/WaSPET/GriDAT/exdaily.db') # create database called exdaily. experimental daily data. maybe should be semi monthly?
    graceTWS = read_sql(sqlStmnt,con)
    # print(graceTWS)
    con.close()
    return(graceTWS)

def namesOfTrip(tripList,element=None):
    trip = ','.join(tripList)                       # combine into comma separated list
    baseUrl = 'https://wcc.sc.egov.usda.gov/awdbRestApi/services/v1/stations?'
    url = f'{baseUrl}stationTriplets={trip}'
    if element:
        url = f'{url}&elements={element}'
    req = requests.get(url)                     # send request for data
    res = req.json()                            # get requested data in json fmt (list of stations)
    nameList = [i['name'].replace(' ','') for i in res] # pull out names, no spaces
    tripList = [i['stationTriplet'] for i in res]   # get just triplets in list
    tripDf = DataFrame({'trip':tripList,'name':nameList})
    return(tripDf)

def getForecast(wy,tripDf):
    '''
    function to get forecasts for triplet list for defined water year
    returns pandas dataframe
    '''
    baseUrl = 'https://wcc.sc.egov.usda.gov/awdbRestApi/services/v1/forecasts?stationTriplets='    
    tripTxt = ','.join(tripDf['trip'].tolist())                       # combine into comma separated list
    ele =  '&elementCodes=SRVO'
    bpd = f'&beginPublicationDate={wy}-01-01'
    epd = f'&endPublicationDate={wy}-09-01'
    xcd =  '&exceedenceProbabilities=10%2C30%2C50%2C70%2C90'
    # reqUrl = f'{baseUrl}{tripTxt}{ele}{bpd}{epd}'
    reqUrl = f'{baseUrl}{tripTxt}{ele}{bpd}{epd}{xcd}'
    # print(reqUrl)
    dReq = requests.get(reqUrl)                 # send request for data
    dRes = dReq.json()                          # get requested data in json fmt (list of stations)
    if not dRes:                                # if no obs
        return(DataFrame())                     # return an empty DataFrame
    for i,li in enumerate(dRes):
        liName = tripDf.loc[tripDf.trip==li['stationTriplet'],'name'].values[0] # extract name from tripDf using triplet
        for j,d in enumerate(li['data']):
            dCode = d['elementCode']
            fDf0 = DataFrame(d['forecastValues'],index=[d['publicationDate']])
            fp = dateNum2dateStr(d['forecastPeriod'])
            columnNames = [f'{liName}_{fp}_p{x}' for x in fDf0.columns] # construct column names
            fDf0.columns = columnNames          # replace with new column names
            if (i==0)&(j==0):                   # if first round...
                fDf=fDf0.copy(deep=True)        # create master df
            else:                               # invoke the trickery of combination
                if fDf0.index[0] in fDf.index:  # if index already exists
                    if columnNames[0] in fDf.columns: # if column already exists, 
                        fDf.update(fDf0)        # over write missing data (avoids suffixes from merge)
                    else:                       # if col doesn't exist..
                        fDf = fDf.merge(fDf0,left_index=True,right_index=True,how='outer') # use the merge
                else:                           # if index is new...
                    fDf = concat([fDf,fDf0])    # ...just concat
    index0 = fDf.index.to_list()                # fix 16th of month pub dates. indexes are immutable, so make into a list to alter. as soon as db is fixed, we can remove this chunk of code
    for i in range(len(index0)):
        index0[i] = index0[i].replace('-16 ','-15 ')
    fDf.index = to_datetime(index0)             # but not this step. needs to be a datetime index to merge in other code.
    fDf = fDf*1000                              # convert kaf to af
    if (fDf.index[1]-fDf.index[0]) < timedelta(days=25):  # see if forecasts are monthly or semimonthly
        dur = 'SEMIMONTHLY'                # save appropriate 'druation'
    else:
        dur = 'MONTHLY'
    return(fDf,dur)            

def dateNum2dateStr(dateList):
    tt0 = dateList[0]           # forecast period start
    fps = dt(2024,int(tt0[:2]),int(tt0[-2:])).strftime('%b').upper()  # forecast period start, year doesn't matter
    if int(tt0[-2:])==15:       # if mid month
        fps=f'{fps[:1]}15'      # rename forecast period start with 15: J15 instead of JAN
    tt0 = dateList[1]           # forecast period end
    fpe = dt(2024,int(tt0[:2]),int(tt0[-2:])).strftime('%b').upper()  # forecast period end, year doesn't matter
    if fps==fpe:                # if start time and end time are the same
        fp=fps                  # forecast period is just the month name
    else:                       # ... if they are different
        fp = f'{fps}-{fpe}'     # construct forecast period with start and end month
    return(fp)

#---------------------------POSSIBLY MOVE FOLLOWING FUNCTIONS TO EVALUATION TOOLBOX MODULE ??? ----------------------------
def plotZscore(df):
    '''
    function to plot normalized data, meant for predictor and target vars
    loops through columns, normalizes, then plots
    '''
    from matplotlib import pyplot as plt
    for column in df.columns: 
        df[column] = (df[column] - df[column].mean()) / df[column].std() 
    df.plot()
    plt.show()

def compareFcst(dirLst):
    '''
    function to compare skill metrics between 2 model runs (different run directories)
    '''
    def nameFromDir(dir):
        splitterLst = dir.split('/')
        endTxtSplit = splitterLst[-1].split('_')
        # print(endTxtSplit)
        if endTxtSplit[-1]=='ga':
            gaTxt = 'Ga'
            ttTime = endTxtSplit[-2]
            siteTxt = endTxtSplit[-3]
            # print(ttTime)
        else:
            gaTxt = ''
            ttTime = endTxtSplit[-1]
            siteTxt = endTxtSplit[-2]
        # exit(0)
        fcstTime = dt.strptime(ttTime,'%m-%d')
        fcstTimeTxt = dt.strftime(fcstTime,'%b%d')
        name = f'{siteTxt}_{fcstTimeTxt}_{splitterLst[-2]}{gaTxt}'
        return(name)

    ensSPRS = 'ensemble_StandardPerformanceReportingSuite.csv'

    firstModelLoopFlag = 1
    for d in dirLst:
        # print(d)
        name0 = nameFromDir(d)
        ens0fn=os.path.join(d,ensSPRS)
        ensDf0 = read_csv(ens0fn,index_col=1)
        ensDf0.drop(columns=['Unnamed: 0'],inplace=True)
        ensDf0.rename(columns={'values':name0},inplace=True)
        # print(ensDf0)
        # continue
        if firstModelLoopFlag == 1:
            ensDf = ensDf0.copy(deep=True)
            firstModelLoopFlag = 0
        else:
            ensDf = ensDf.merge(ensDf0,right_index=True,left_index=True)
        # exit(0)    
    return(ensDf)

def modelChrom(dir0,plot=True):
    '''
    function to pull out data on which predictor variables were retained by the genetic algorithm
        input:  dir0: run directory of model run with ga
                plot: boolean whether to plot data
        output: data frame ane plot of binary chromosomes and PC's retained
    '''
    from matplotlib import pyplot as plt

    ########## get info on number of predictor vars used ##########
    trainingDf = read_csv(os.path.join(dir0,'MMPEInputData_ModelBuildingMode.txt'),sep='\t',index_col=0)
    # print(trainingDf)
    predVarNames = list(trainingDf)[1:]
    # print(predVarNames)
    pltTitle = dir0.split('/')
    pltTitle = f'{pltTitle[-1]}: {pltTitle[-2]}'
    fileList = os.listdir(dir0)
    fileList = [x for x in fileList if 'GA_RunSummary' in x]
    fileList.sort()
    modelNames = [x.split('_')[-1].rstrip('.txt') for x in fileList]
    firstModelLoopFlag=1                                # set first loop flag high
    for fnum,file0 in enumerate(fileList):              # for each model
        # print(file0)
        # print(modelNames[fnum])
        with open(os.path.join(dir0,file0),'r') as f:   # open ga summary file
            lines = f.read().splitlines()               # read in contents as variable
            for l in lines:                             # loop through lines in text file
                if 'Best Solution' in l:                # search for text
                    # print(f'{file0}: {l}')
                    chro = l[18:-1]                     # get binary data, which predictor was used.
                    # print(chro)
                    chro = [int(ele) for ele in chro.split()] # get data into list
                    # print(chro)
                    # exit(0)
                    if firstModelLoopFlag == 1:                             # not sure why this flag is here.
                        for pcName in range(len(chro)-len(predVarNames)):
                            predVarNames.append(f'PC{pcName+2}')
                        chroDf = DataFrame([chro],columns=predVarNames,index=modelNames) # create dataframe, all data is the same
                        firstModelLoopFlag = 0                                              # set first loop flag low
                    else:
                        chroDf.loc[modelNames[fnum],:] = chro
    if plot==False:                             # if no plot requested
        return(chroDf)                          # return chromosome dataframe and exit
    yaxlabs = chroDf.index.to_list()
    yaxlabs.reverse()
    ys = np.flip(np.arange(len(chroDf.index)))
    xs = np.arange(len(chroDf.columns))
    fig = plt.figure(figsize=(10,7))
    ax0 = fig.add_subplot(position=[.1,.23,.85,.7])
    for r in range(chroDf.shape[0]):
        ydat = np.repeat(ys[r],len(chroDf.columns))
        sizee=chroDf.iloc[r,:].values * 300
        ax0.scatter(xs,ydat,s=sizee)
    ax0.set_xticks(np.arange(len(chroDf.columns)),chroDf.columns,rotation='vertical')
    ax0.set_yticks(np.arange(len(chroDf.index)),yaxlabs)
    plt.title(pltTitle)
    plt.grid()
    plt.show()
    return(chroDf)

def predictorInfluence(dir0):
    '''
    calculate the influence of a predictor variable for a forecast model
    looks to see if models use predictor in question (usually experimental), 
        then looks at the retained eigenvector modes for each model and how 
        much the variable influences that mode.
    input: is model run directory
    '''
    
    chroDf = modelChrom(dir0,plot=False) # get chromosomes from ga run. models retained 

    # print(chroDf)
    ### for each model ###
    for mod0,pred0 in chroDf.iterrows():

        predPcRetained = [chroDf.columns[x] for x in range(chroDf.shape[1]) if chroDf.loc[mod0,chroDf.columns[x]]]
        # print(predPcRetained)
        # print(f'PC2' in predPcRetained)
        # exit(0)
        eigSpecFn = f'{mod0}_eigenspectrum.csv'
        eigSpec = read_csv(os.path.join(dir0,eigSpecFn))
        eigSpec.rename(columns={'Unnamed: 0':'pc'},inplace=True)
        eigSpec.set_index('pc',inplace=True)
        print(eigSpec)

        eigVecFn = f'{mod0}_eigenvector.csv'
        eigVec = read_csv(os.path.join(dir0,eigVecFn)) # The equation for PC1 is -0.398X1 + 0.439X2 + 0.653X3 - 0.449X4 +0.14X5, where X_N is the Nth predictor. 
        eigVec.rename(columns={'Unnamed: 0':'pc'},inplace=True)
        eigVec.set_index('pc',inplace=True)
        eigVec = eigVec.set_axis(predPcRetained[:eigVec.shape[1]],axis=1) # tricky column rename
        print(eigVec)

        for e0,pve in eigSpec.iterrows():

            if (e0==1) | (f'PC{e0}' in predPcRetained):
                # print(f'\nPC{e0}: perc_var_expl: {pve.values[0]}\n')
                # print(f'sum: {sum(abs(eigVec.loc[e0]))}')
                pcWts = eigVec.loc[e0]/sum(abs(eigVec.loc[e0]))
                # print(f'wts for eig vec {e0}: \n{pcWts}')
                combPcWts = pcWts * pve.values[0]
                combPcWts = combPcWts.to_frame()
                combPcWts.rename(columns={e0:f'{mod0}_PC{e0}'},inplace=True)
                print(f'\ntotal influence for model {mod0}: \n{combPcWts}')
                # print(sum(abs(combPcWts)))
                if e0==1: 
                    influenceDf = combPcWts.copy(deep=True) # create influence dataFrame
                else:
                    influenceDf = influenceDf.merge(combPcWts,left_index=True,right_index=True,how='outer')
        print(influenceDf) # good but has negatives. need to sum across rows and combine into one dataframe. alert for neg.
        exit(0)
        
        
        pvIndex = [x for x in pred0.index if not 'PC' in x]
        print(pvIndex)
        pcIndex = [x for x in pred0.index if 'PC' in x]             # get list of addl PC's in chromosome
        pcLst = [1] + [int(x[-1]) for x in pcIndex if pred0[x]==1]  # get list of PC's used in this model (always retains first one)
        # for p in pcLst:                                             # for each PC used in that model
