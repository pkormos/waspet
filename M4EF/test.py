import M4EF as m4ef

optionsDict = {
    'stnTriplet':         '06099500:MT:USGS', # station triplet
    'fcstMo':             9,                  # forecast month int 1=January
    'fcstDay':            1,                  # forecast day int. 1=1st, 15=15th
    'trainingStartYear':  2003,               # start calibration year
    'trainingEndYear':    2024,               # end calibration year
    'fcstPdSt':           4,                  # april  This is the runoff period we are forecasting
    'fcstPdEt':           7,                  # july   This is the runoff period we are forecasting
    'wteqStnLst':         ['307:MT:SNTL','458:MT:SNTL','613:MT:SNTL','13A05:MT:SNOW','12A01:MT:SNOW'], # list of swe stns to be used in training
    'precStnLst':         ['307:MT:SNTL'],#'693:MT:SNTL','458:MT:SNTL','613:MT:SNTL'], # list of precipitation stations to be used in training
    # 'evalData':           None,               # name of data to be evaluated in forecasting framework 
    'evalData':           'grace_tws',        # name of data to be evaluated in forecasting framework 
    'ga':                 True,
    'm4baseDir':          'D:/projects/M4/graceEval/'
}


# marias0 = m4ef.evalFcstPt(optionsDict)
# df0 = marias0.pullData()
# print(df0)
# # df1 = marias0.m4setup(clobber=True)
# # print(df1)
# # marias0.runM4()
# exit(0)

# m4ef.plotZscore(df0)

# dir0 = 'D:/projects/M4/graceEval/base/MariasRnrShelby_2-1'
# dir1 = 'D:/projects/M4/graceEval/grace_tws/MariasRnrShelby_2-1'
# m4ef.compareFcst(dir0,dir1)
# graceApr = m4ef.getGrace(optionsDict['stnTriplet'],optionsDict['fcstMo'],optionsDict['fcstDay'],var='TWDSGZZ',sy=2003,ey=2024)
# print(graceApr)

# ###################### PLOT CODE FOR VARS IN DB ######################
# import sqlite3
# from pandas import read_sql,to_datetime
# var='TWDSGZZ'
# station = optionsDict['stnTriplet'].split(':')[0]
# state = optionsDict['stnTriplet'].split(':')[1]
# sqlStmnt = f"Select cal_yr,mo,day,val FROM exdaily WHERE shef='{var}' AND station='{station}' AND state='{state}';"
# # sqlStmnt = f"DELETE FROM exdaily WHERE shef='{var}' AND station='{station}' AND state='{state}';"
# con=sqlite3.connect('D:/projects/WaSPET/GriDAT/exdaily.db') # create database called exdaily. experimental daily data. maybe should be semi monthly?
# # cur = con.cursor()                  # db cursor to execute SQL statements and fetch results from SQL queries
# # cur.execute(sqlStmnt) # table already exists.
# # con.commit()
# graceTWS = read_sql(sqlStmnt,con)
# graceTWS.rename(columns={'cal_yr':'year','mo':'month'},inplace=True)
# graceTWS['date'] = to_datetime(graceTWS[['year','month','day']])
# con.close()
# print(graceTWS.head())
# from matplotlib import pyplot as plt
# plt.plot(graceTWS.date,graceTWS.val,'.b')
# plt.show()
# exit(0)
# ##################################################################

dir0 = 'D:/projects/M4/graceEval/grace_tws/MariasRnrShelby_2-1_ga'
# # dir0 = 'D:/projects/M4/graceEval/base/MariasRnrShelby_2-1_ga'
# m4ef.modelChrom(dir0)

m4ef.predictorInfluence(dir0)