WaSPET: Water Supply Prediction Evaluation Toolset

This toolset has 2 components: GriDAT is focused on getting values aggregated from gridded datasets for water supply basins.
							   M4EF is focused on evaluting those values as predictor variables in the M4 modeling framework.