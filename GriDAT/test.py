# import sqlite3
# from pandas import read_sql
# sql0 = "Select * FROM exdaily WHERE cal_yr>=2022"  
# con=sqlite3.connect('exdaily.db')   # create database called exdaily. experimental daily data. maybe should be semi monthly?
# df = read_sql(sql0,con)
# print(df)
# exit(0)

# # cur = con.cursor()
# # dat = cur.execute(sql0)
# # print(dat.fetchall())
# con.close()
# exit(0)

import GriDAT as gd
fcstTripLst = ['06099500:MT:USGS'] # marias
# fcstTripLst = ['06192500:MT:USGS', '06207500:MT:USGS', '06707500:CO:USGS'] # Yellowstone, Clark Fork, and South Platte
# tl = ['05016000:MT:USGS', '05017500:MT:USGS']
# testGriDAT = gd.gridMask(tripLst=tl)

##############################
### TEST .graceHistorical ###
# dataDir = 'D:/projects/spatialData/GLDAS_CLSM025_DA1/conusNcFiles'
# graceDf = testGriDAT.graceHistorical(histDataDir=dataDir,updateDB=True,format='data')
# graceDf = testGriDAT.graceHistorical(histDataDir=dataDir,updateDB=False,format='data')
# print(graceDf.head())
# from matplotlib import pyplot as plt
# graceDf.plot()
# plt.show()
##############################

testGriDAT = gd.gridMask(tripLst=fcstTripLst)
df = testGriDAT.graceSubSurface(sy=2003,ey=2003,updateLocalData=True,updateDB=False)

