'''
created by:     Patrick Kormos
    started:    2024 June 18
'''

import geopandas as gpd
import requests,os,time
from pandas import DataFrame,to_datetime
import numpy as np
import xarray as xr

class gridMask:

    def __init__(self,tripLst=None,shpFile=None):
        '''
        initiate class function. sets up the geographic areas where gridded data will be aggregated to. 
            inputs:     tripLst:    list of station triplets of water supply points. These points will be selected from the master FCST AOI polygon shapefile
                        shpFile:    a custom shapefile of areas of interest where gridded data will be aggregated to. all AOI's in shapefile will be used
        '''

        # get geopandas from master AOI shapefile, just basins from given tripLst
        if tripLst:
            allWsupShapefile = 'D:/projects/spatialData/master_FCST_AOI_polygons/master_FCST_AOI_polygons.shp'
            # stationTri IN ('06099500:MT:USGS','06192500:MT:USGS','06207500:MT:USGS','06707500:CO:USGS')
            sql0 = f"stationTri IN ('{'&&'.join(tripLst)}')" # 2 step string process works with .venv and conda openDap environments
            sql0 = sql0.replace('&&',"','")                  # 2 step string process works with .venv and conda openDap environments
            gdf = gpd.read_file(allWsupShapefile,where=f"{sql0}")
        # else: # get geopandas df from given shapefile, none of this is robust. it would be very shapefile dependent
        #     gdf = gpd.read_file(shpFile)
        #     gdf.rename(columns={'name_y':'name'},inplace=True)
        #     tripLst = gdf['stationTri'].to_list()
        #     print(tripLst)
        #     # exit(0)
        trip = ','.join(tripLst)                       # combine into comma separated list to get names of basins from trips
        baseUrl = 'https://wcc.sc.egov.usda.gov/awdbRestApi/services/v1/stations?'
        url = f'{baseUrl}stationTriplets={trip}'
        req = requests.get(url)                     # send request for data
        res = req.json()                            # get requested data in json fmt (list of stations)
        nameList = [i['name'].replace(' ','') for i in res] # pull out names, no spaces
        tripList = [i['stationTriplet'] for i in res]   # get just triplets in list
        tripDf = DataFrame({'trip':tripList,'name':nameList})
        self.gdf = gdf.merge(tripDf,left_on='stationTri',right_on='trip',how='outer')
        self.gdf.drop(columns=['stationTri','bagis_note'],inplace=True)
        self.gdf.set_index('trip',inplace=True)
        ### didn't need this step 8/8/2024.... did need this step 8/9/2024
        self.gdf.set_crs(epsg=6350,inplace=True,allow_override=True) # replace ridiculous crs info with simple EPSG... allows for to_crs function
        ##################################################################

    def graceHistorical(self,histDataDir,varLst=['TWS_tavg'],format='db',updateDB=False):
        '''
        function for getting basin avg. grace data 
            inputs:     histDataDir: directory with GLDAS netcdf files
                        varLst:      list of GLDAS variables to aggregate, defaults to terrestrial water storage
                            !!! NOT ROBUST, HARD CODED FOR TWS_tavg !!!! fix if needed. else just use graceThredds function below
                        format:     db: database format with station,state,network,shef, cal_yr, mo, day, val columns
                                    data: data format with time as index and value columns.  columns named by station
                        updateDB:   whether to update sql database with values, use False to only return dataFrame

            returns:    Data frame of basin avg GLDAS variable
        '''
        import rasterio as rio
        import rioxarray

        histFileLst = os.listdir(histDataDir)                              # get file list from given data dir
        histFileLst = [x for x in histFileLst if x.endswith('.nc4')]    # limit to netCDF files
        absHistFileLst = [f'{histDataDir}/{x}' for x in histFileLst]    # create full paths to each file for xarray
        tws = xr.open_mfdataset(absHistFileLst,engine='rasterio',variable=varLst,combine ='nested',concat_dim='time') #data_vars='TWS_tavg',
        tws.rio.set_spatial_dims(x_dim='x',y_dim='y',inplace=True)
        tws.rio.write_crs("epsg:4326", inplace=True)
        self.gdf.to_crs('epsg:4326',inplace=True)
        for i,b in self.gdf.iterrows():
            # print(b)
            print(f"BASIN:  {b['name']}")
            mskd = tws.rio.clip([b.geometry],all_touched=True)      # clip where all cells touched by basin are used (more pixels)
            mskdMean = mskd.mean(dim=['x','y'],skipna=True)         # get basin mean for each time
            graceDf0 = mskdMean.to_dataframe()
            graceDf0.drop(columns=['spatial_ref'],inplace=True)
            graceDf0.rename(columns={'TWS_tavg':f"{b['name'].replace(' ','')}"},inplace=True)
            if i == 0:
                graceDf=graceDf0.copy(deep=True)
            else:
                graceDf = graceDf.merge(graceDf0,left_index=True,right_index=True,how='outer')
            del graceDf0
        graceDf_dataFormat = graceDf.copy(deep=True)
        if (updateDB==True) or (format=='db'):
            for junk,record in self.gdf.iterrows():
                graceDf.rename(columns={record['name']:record.trip},inplace=True)   # rename each column with triplet
            graceDf.index = graceDf.index.to_datetimeindex()
            graceDf=graceDf.stack().to_frame()  # make one row for each column value (for more than one site)
            graceDf.reset_index(inplace=True)   # get rid of multi index, move to columns
            graceDf[['station','state','network']] = graceDf['level_1'].str.split(':', expand=True)
            graceDf['cal_yr'] = graceDf['level_0'].dt.year
            graceDf['mo'] = graceDf['level_0'].dt.month
            graceDf['day'] = graceDf['level_0'].dt.day
            graceDf.drop(['level_0','level_1'],axis=1,inplace=True)
            graceDf.rename(columns={0:'val'},inplace=True)
            graceDf.loc[:,'shef']='TWDSGZZ'     # shef TW=terrestrial water D=daily S=simulated G=Goddard 
            graceDf['network']='GLDAS'
        if updateDB:
            import sqlite3
            con=sqlite3.connect('exdaily.db')   # create database called exdaily. experimental daily data. maybe should be semi monthly?
            cur = con.cursor()                  # db cursor to execute SQL statements and fetch results from SQL queries
            # cur.execute("CREATE TABLE promonly(station,state,network,shef, cal_yr, mo, day, val)") # table already exists.
            # con.commit()
            graceDf.to_sql('exdaily',con=con,if_exists='append',index=False)#,dtypes={'station':int32,})
            con.close()
        
        if format=='db':
            return(graceDf)
        elif format=='data':
            return(graceDf_dataFormat)        

    def graceThredds(self,sy,ey,var='TWS_tavg',updateLocalData=False,updateDB=False,dataFormat='data'):
        '''
        function to aggregate GLDAS variable by basin and store to db
        inputs:     sy:              start year int
                    ey:              end year int
                    var:             GLDAS variable
                    updateLocalData: whether to update basin grids on local hard drive
                    updateDB:        whether to update values in DB
                    dataFormat:      if returning dataframe, whether you want data 'format' or 'sql' format
        returns:    pandas dataframe
        '''

        import rioxarray as rxr
        dataDirBase = 'D:/projects/spatialData/GLDAS_CLSM025_DA1/'

        self.gdf.to_crs('epsg:4326',inplace=True)   ##### REPROJECT allWsupShapefile SHAPEFILE TO GRACE DATA PROJECTION
        tt = self.gdf.bounds                        # temp df with bounds
        tt.set_index(self.gdf.index,inplace=True)   # set index for easy merge
        self.gdf = self.gdf.merge(self.gdf.bounds,left_index=True,right_index=True,how='outer') # merge in bounds for each basin in shapefile
        for cal_yr in range(sy,ey+1):
            print(cal_yr)
            if updateLocalData:                         # update local data on hard drive
                ### could improve this to only get data that isn't there already and append that to nc file, would need a force replace maybe
                URL=f'https://hydro1.gesdisc.eosdis.nasa.gov/thredds/dodsC/GLDAS_aggregation/GLDAS_CLSM025_DA1_D.2.2/GLDAS_CLSM025_DA1_D.2.2_Aggregation_{cal_yr}.ncml'
                print('attempt file open from thredds server')
                try:
                    ds=xr.open_dataset(URL,decode_coords="all")     # open dataset, thredds server, all days in cal year
                    lat = ds.coords['lat'][:]
                    lon = ds.coords['lon'][:]
                except:
                    print('dataset read not successful.')
                    print('please use conda openDap environment to run this code')
                    exit(0)
                for i,bas in self.gdf.iterrows():
                    print(bas['name'])
                    print('loading grids')
                    st = time.time()
                    aoiGrids = ds.sel(lat=slice(bas.miny,bas.maxy),lon=slice(bas.minx,bas.maxx))[var].load() # get all times for this basin for this year #,time=slice(time1,time2)
                    et = time.time()
                    print(f'done loading grids: {et-st} seconds, {(et-st)/60} mins.')
                    aoiGrids = aoiGrids.chunk(aoiGrids.shape)
                    # print(aoiGrids.chunksizes)
                    # print(aoiGrids[:,:,:].values)
                    ncDirNameOut = f"{bas['name']}_GLDAS_CLSM025_DA1_D.2.2"
                    ncFileNameOut = f"{bas['name']}_{var}_{cal_yr}.nc"
                    if not os.path.exists(os.path.join(dataDirBase,ncDirNameOut)): 
                        os.makedirs(os.path.join(dataDirBase,ncDirNameOut)) 
                    aoiGrids.to_netcdf(os.path.join(dataDirBase,ncDirNameOut,ncFileNameOut))# save to local disk
            if updateDB:    
                import sqlite3
                '''
                Do the clipping and aggregating. basin mean. pixel center in basin
                '''
                flf = 1
                for i,bas in self.gdf.iterrows():
                    ncDirNameOut = f"{bas['name']}_GLDAS_CLSM025_DA1_D.2.2"
                    ncFileNameOut = f"{bas['name']}_{var}_{cal_yr}.nc"
                    bfn = os.path.join(dataDirBase,ncDirNameOut,ncFileNameOut)
                    bfn = f'netcdf:{bfn}:{var}'
                    with xr.open_dataset(bfn,engine='rasterio',variable=var) as src:
                        src.rio.set_spatial_dims(x_dim='x',y_dim='y',inplace=True)
                        src.rio.write_crs("epsg:4326", inplace=True)
                        print(f'opened {var} data for {cal_yr}')
                        mskd = src.rio.clip([bas.geometry])#,all_touched=True)      # clip where all cells touched by basin are used (more pixels)
                        # print(mskd[var][:10,:,:])
                        mskdMean = mskd.mean(dim=['x','y'],skipna=True).to_dataframe()         # get basin mean for each time
                        mskdMean.drop(columns=['spatial_ref'],inplace=True)
                        mskdMean.rename(columns={var:f"{bas['name'].replace(' ','')}"},inplace=True)
                        # mskdMean.rename(columns={'TWS_tavg':f"{bas['name'].replace(' ','')}"},inplace=True)
                        if flf==1:                        
                            aoiDf = mskdMean.copy(deep=True)
                            flf=0
                        else:
                            aoiDf = aoiDf.merge(mskdMean,left_index=True,right_index=True,how='outer')
                # aoiDf_data = aoiDf.copy(deep=True)

                '''
                Update database
                '''
                for trip0,record0 in self.gdf.iterrows():
                    aoiDf.rename(columns={record0['name']:trip0},inplace=True)   # rename each column with triplet
                aoiDf.index = aoiDf.index.to_datetimeindex()
                aoiDf=aoiDf.stack().to_frame()  # make one row for each column value (for more than one site)
                aoiDf.reset_index(inplace=True)   # get rid of multi index, move to columns
                aoiDf[['station','state','network']] = aoiDf['level_1'].str.split(':', expand=True)
                aoiDf['cal_yr'] = aoiDf['level_0'].dt.year
                aoiDf['mo'] = aoiDf['level_0'].dt.month
                aoiDf['day'] = aoiDf['level_0'].dt.day
                aoiDf.drop(['level_0','level_1'],axis=1,inplace=True)
                aoiDf.rename(columns={0:'val'},inplace=True)
                if var == 'TWS_tavg':
                    aoiDf.loc[:,'shef']='TWDSGZZ'     # shef TW=terrestrial water D=daily S=simulated G=Goddard 
                    aoiDf['network']='GLDAS'
                else:
                    print(f'need to define shef code for {var} in graceThredds function.')
                    exit(0)
                con=sqlite3.connect('D:/projects/WaSPET/GriDAT/exdaily.db')   # create database called exdaily. experimental daily data. maybe should be semi monthly?
                cur = con.cursor()                  # db cursor to execute SQL statements and fetch results from SQL queries
                # cur.execute("CREATE TABLE promonly(station,state,network,shef, cal_yr, mo, day, val)") # table already exists.
                # con.commit()
                aoiDf.to_sql('exdaily',con=con,if_exists='append',index=False)#,dtypes={'station':int32,})
                con.close()
            
            # return(aoiDf)
              
    def graceSubSurface(self,sy,ey,updateLocalData=False,updateDB=False,dataFormat='data'):
        '''
        function to add GLDAS ground water storage and profile soil moisture, then aggregate by basin and store to db
        inputs:     sy:              start year int
                    ey:              end year int
                    updateLocalData: whether to update basin grids on local hard drive
                    updateDB:        whether to update values in DB
                    dataFormat:      if returning dataframe, whether you want data 'format' or 'sql' format
        returns:    pandas dataframe
        '''

        import rioxarray as rxr
        dataDirBase = 'D:/projects/spatialData/GLDAS_CLSM025_DA1/'

        self.gdf.to_crs('epsg:4326',inplace=True)   ##### REPROJECT allWsupShapefile SHAPEFILE TO GRACE DATA PROJECTION
        tt = self.gdf.bounds                        # temp df with bounds
        tt.set_index(self.gdf.index,inplace=True)   # set index for easy merge
        self.gdf = self.gdf.merge(self.gdf.bounds,left_index=True,right_index=True,how='outer') # merge in bounds for each basin in shapefile
        for cal_yr in range(sy,ey+1):
            print(cal_yr)
            if updateLocalData:                         # update local data on hard drive
                ### could improve this to only get data that isn't there already and append that to nc file, would need a force replace maybe
                URL=f'https://hydro1.gesdisc.eosdis.nasa.gov/thredds/dodsC/GLDAS_aggregation/GLDAS_CLSM025_DA1_D.2.2/GLDAS_CLSM025_DA1_D.2.2_Aggregation_{cal_yr}.ncml'
                print('attempt file open from thredds server')
                try:
                    ds=xr.open_dataset(URL,decode_coords="all")     # open dataset, thredds server, all days in cal year
                    lat = ds.coords['lat'][:]
                    lon = ds.coords['lon'][:]
                except:
                    print('dataset read not successful.')
                    print('please use conda openDap environment to run this code')
                    exit(0)
                for i,bas in self.gdf.iterrows():
                    print(bas['name'])
                    print('loading grids')
                    # time1 = "2003-02-01T00:00:00.000000000"
                    # time2 = "2003-02-10T00:00:00.000000000"
                    st = time.time()
                    aoiGrids = ds.sel(lat=slice(bas.miny,bas.maxy),lon=slice(bas.minx,bas.maxx))[['GWS_tavg','SoilMoist_P_tavg']].load()          # get all times for this basin for this year #,time=slice(time1,time2)
                    et = time.time()
                    print(f'done loading grids: {et-st} seconds, {(et-st)/60} mins.')
                    aoiGrids = aoiGrids.assign(subSurfaceContent=aoiGrids['GWS_tavg']+aoiGrids['SoilMoist_P_tavg'])
                    aoiGrids = aoiGrids.subSurfaceContent.chunk(aoiGrids.subSurfaceContent.shape)
                    ncDirNameOut = f"{bas['name']}_GLDAS_CLSM025_DA1_D.2.2"
                    ncFileNameOut = f"{bas['name']}_subSurfaceContent_{cal_yr}.nc"
                    if not os.path.exists(os.path.join(dataDirBase,ncDirNameOut)): 
                        os.makedirs(os.path.join(dataDirBase,ncDirNameOut)) 
                    aoiGrids.to_netcdf(os.path.join(dataDirBase,ncDirNameOut,ncFileNameOut))# save to local disk
            if updateDB:    
                import sqlite3
                '''
                Do the clipping and aggregating. basin mean. pixel center in basin
                '''
                flf = 1
                for i,bas in self.gdf.iterrows():
                    ncDirNameOut = f"{bas['name']}_GLDAS_CLSM025_DA1_D.2.2"
                    ncFileNameOut = f"{bas['name']}_subSurfaceContent_{cal_yr}.nc"
                    bfn = os.path.join(dataDirBase,ncDirNameOut,ncFileNameOut)
                    bfn = f'netcdf:{bfn}:subSurfaceContent'
                    with xr.open_dataset(bfn,engine='rasterio',variable='subSurfaceContent') as src:
                        src.rio.set_spatial_dims(x_dim='x',y_dim='y',inplace=True)
                        src.rio.write_crs("epsg:4326", inplace=True)
                        print(f'opened subSurfaceContent data for {cal_yr}')
                        mskd = src.rio.clip([bas.geometry])#,all_touched=True)      # clip where all cells touched by basin are used (more pixels)
                        # print(mskd[var][:10,:,:])
                        # exit(0)
                        mskdMean = mskd.mean(dim=['x','y'],skipna=True).to_dataframe()         # get basin mean for each time
                        mskdMean.drop(columns=['spatial_ref'],inplace=True)
                        mskdMean.rename(columns={'subSurfaceContent':f"{bas['name'].replace(' ','')}"},inplace=True)
                        if flf==1:                        
                            aoiDf = mskdMean.copy(deep=True)
                            flf=0
                        else:
                            aoiDf = aoiDf.merge(mskdMean,left_index=True,right_index=True,how='outer')
                # aoiDf_data = aoiDf.copy(deep=True)

                '''
                Update database
                '''
                for trip0,record0 in self.gdf.iterrows():
                    aoiDf.rename(columns={record0['name']:trip0},inplace=True)   # rename each column with triplet
                aoiDf.index = aoiDf.index.to_datetimeindex()
                aoiDf=aoiDf.stack().to_frame()  # make one row for each column value (for more than one site)
                aoiDf.reset_index(inplace=True)   # get rid of multi index, move to columns
                aoiDf[['station','state','network']] = aoiDf['level_1'].str.split(':', expand=True)
                aoiDf['cal_yr'] = aoiDf['level_0'].dt.year
                aoiDf['mo'] = aoiDf['level_0'].dt.month
                aoiDf['day'] = aoiDf['level_0'].dt.day
                aoiDf.drop(['level_0','level_1'],axis=1,inplace=True)
                aoiDf.rename(columns={0:'val'},inplace=True)
                aoiDf.loc[:,'shef']='SSDSGZZ'     # shef SS=sub surface D=daily S=simulated G=Goddard 
                aoiDf['network']='GLDAS'
                con=sqlite3.connect('D:/projects/WaSPET/GriDAT/exdaily.db')   # create database called exdaily. experimental daily data. maybe should be semi monthly?
                cur = con.cursor()                  # db cursor to execute SQL statements and fetch results from SQL queries
                aoiDf.to_sql('exdaily',con=con,if_exists='append',index=False)#,dtypes={'station':int32,})
                con.close()
            
            # return(aoiDf)
              




def searchClosest(target,vector):
    '''
    unused
    '''
    # print(target)
    # print(vector)
    distance = abs(vector-target)
    # print(distance)
    ind = np.where(distance==min(distance))
    # print(ind)
    return(ind[0][0]) 
